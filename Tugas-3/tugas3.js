// Soal 1 
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
console.log(kataPertama+" "+kataKedua[0].toUpperCase()+kataKedua.substring(1)+" "+kataKetiga+" "+kataKeempat.toUpperCase());

// Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
console.log(Number(kataPertama)+Number(kataKedua)+Number(kataKetiga)+Number(kataKeempat));

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4
var nilai = 75;
var indeks;

if(nilai>=80){
    indeks = 'A';
}
else if(nilai<80 && nilai>=70){
    indeks = 'B';
}
else if(nilai<70 && nilai>=60){
    indeks = 'C';
}
else if(nilai<60 && nilai>=50){
    indeks = 'D';
}
else{
    indeks = 'E';
}

console.log(indeks);

// Soal 5 
var tanggal = 19;
var bulan = 7;
var tahun = 2001;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;
    default:
        bulan = "Nomor Bulan Tidak Ada"
  }

  console.log(tanggal + " " + bulan + " " + tahun);