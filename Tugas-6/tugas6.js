// Soal 1
const luas = (r) => {
    let pi = 3.14;
    return pi*r*r;
}

const keliling = (r) => {
    let pi = 3.14;
    return pi*r*2;
}

console.log(luas(7));
console.log(keliling(7));

// Soal 2
let kalimat = ""
const tambahKalimat = (kata) => {
    kalimat += `${kata} `
}

tambahKalimat("saya");
tambahKalimat("adalah");
tambahKalimat("seorang");
tambahKalimat("web");
tambahKalimat("developer");

console.log(kalimat)

// Soal 3
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName : () => {
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName();

// Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

// Soal 5 
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)