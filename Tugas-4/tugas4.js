// Soal 1
console.log("LOOPING PERTAMA");
var i = 2;
while(i<=20){
    console.log(i + " - I love coding");
    i = i + 2;
}

console.log("LOOPING KEDUA");
var i = 20;
while(i>=2){
    console.log(i + " - I will become a frontend developer");
    i = i - 2;
}

console.log("");

// Soal 2 
for(var i=1;i<=20;i++){
    if(i%3==0 && i%2==1){
        console.log(i + " - I Love Coding");
    }
    else if(i%2==1){
        console.log(i + " - Santai");
    }
    else if(i%2==0){
        console.log(i + " - Berkualitas");
    }
}

console.log("");

// Soal 3
for(var i=0;i<7;i++){
    for(j=0;j<=i;j++){
        process.stdout.write("#");
    }
    console.log();
}

console.log("");

// Soal 4
var kalimat="saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

console.log("");

// Soal 5 
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for(var buah of daftarBuah){
    console.log(buah);
}
